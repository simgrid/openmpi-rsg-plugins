#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include <stddef.h> // for size_t
#include <pthread.h>
#include "ompi/request/request.h" // for ompi_request_t

enum CommandType
{
    NEW_SEND,
    NEW_RECV,
    TERMINATE
};

void rsg_quit();

void run_ompi_runtime_callback_if_needed(void);
void stop_ompi_runtime_callback_if_needed(void);

void spawn_async_actor_if_needed(char *unique_name);
void terminate_async_actors();

void send_command_to_async_actor(char * unique_name, void * command);
void forward_message_to_nonblocking_actor(char * nonblocking_mailbox_name,
                                          char * communication_mailbox_name,
                                          char * ack_mailbox_name,
                                          enum CommandType command_type,
                                          int is_blocking,
                                          void * buffer,
                                          size_t buffer_size,
                                          int expected_source,
                                          int expected_tag,
                                          ompi_request_t * request);

void rsg_sleep(double duration);
void rsg_compute(double nb_flops);

void rsg_send_msg(const char * mailbox_name, const char *buf, size_t buf_len);
void rsg_send_msg_nonblocking(pthread_t * thread, char *mailbox_name, char *buf, size_t buf_len);

void rsg_recv_msg(const char * mailbox_name, char ** buf);
void rsg_recv_msg_nonblocking(pthread_t * thread, char *mailbox_name);

void rsg_test_sendrecv(unsigned int rank);
void rsg_test_sendrecv_nonblocking(pthread_t * thread, unsigned int rank);

void rsg_test_compute(unsigned int rank);

void rsg_test_setglobal(unsigned int value);
unsigned int rsg_test_getglobal();

void insert_gdb_manual_breakpoint();

#ifdef __cplusplus
}
#endif
