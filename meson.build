project('openmpi-rsg-plugins', 'c', 'cpp',
    default_options : ['c_std=c11', 'cpp_std=c++11'])

#####################
# OpenMPI internals #
#####################
ompi_src_path = get_option('ompi_src_path')
ompi_install_path = get_option('ompi_install_path')

ompi_include_dirs = include_directories(
    ompi_src_path,
    ompi_src_path + '/ompi/include',
    ompi_src_path + '/orte/include',
    ompi_src_path + '/opal/include',
    ompi_src_path + '/opal/mca/hwloc/hwloc1117/hwloc/include',
    ompi_src_path + '/opal/mca/event/libevent2022/libevent/include',
    ompi_src_path + '/opal/mca/event/libevent2022/libevent'
)

orte_include_dirs = include_directories(
    ompi_src_path + '/orte/include'
)

dep_numa = dependency('numa')
dep_udev = dependency('libudev')
dep_pciaccess = dependency('pciaccess')
dep_libz = dependency('libzstd')
dep_liborte = dependency('orte')

###############################
# Plugins direct dependencies #
###############################
dep_jansson = dependency('jansson', method: 'pkg-config')

###############
# ODLS plugin #
###############

odls_src = [
    'src/odls/rsg/logging.h',
    'src/odls/rsg/odls_rsg.h',
    'src/odls/rsg/odls_rsg_module.c',
    'src/odls/rsg/odls_rsg_component.c'
]

mca_odls_rsg = shared_library('mca_odls_rsg', odls_src,
    include_directories: [ompi_include_dirs],
    dependencies: [dep_jansson,
        dep_numa, dep_udev, dep_pciaccess, dep_libz, dep_liborte],
)
