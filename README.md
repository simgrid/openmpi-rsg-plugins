[![pipeline status](https://framagit.org/simgrid/openmpi-rsg-plugins/badges/master/pipeline.svg)](https://framagit.org/simgrid/openmpi-rsg-plugins/pipelines)

This repository contains a set of OpenMPI plugins that aims to bridge SimGrid
and OpenMPI. The goal is to allow MPI applications to use the SimGrid
simulation toolkit as a network backend, rather than a *real* network.
This is done thanks to
[Remote SimGrid](https://github.com/simgrid/remote-simgrid).

Quick links:
- [Installation guide](./doc/install.md)
