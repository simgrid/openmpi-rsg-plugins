#pragma once

#ifdef DBG_LOG_ODLS_RSG
    #define debug_printf(fmt, ...) \
        do { fflush(stdout); fflush(stderr); \
            fprintf(stderr, "%s:%d:%s(): " fmt "\n", \
            __FILE__, __LINE__, __func__,  ##__VA_ARGS__); \
            fflush(stdout); fflush(stderr); } while (0)
#else
    #define debug_printf(fmt, ...) do {} while (0)
#endif
