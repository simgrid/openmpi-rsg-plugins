#include <mpi.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size == 2)
    {
        int data = -1;

        MPI_Request requests[2];
        MPI_Status statuses[2];

        if (rank == 0)
        {
            MPI_Isend(&rank, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[0]);
            MPI_Irecv(&data, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[1]);
            MPI_Waitall(2, requests, statuses);
            printf("Rank=%d. Received %d from myself\n", rank, data);
        }
        MPI_Barrier(MPI_COMM_WORLD);

        if (rank == 1)
        {
            MPI_Isend(&rank, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[0]);
            MPI_Irecv(&data, 1, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[1]);
            MPI_Waitall(2, requests, statuses);
            printf("Rank=%d. Received %d from myself\n", rank, data);
        }
        MPI_Barrier(MPI_COMM_WORLD);

        assertf(data == rank, "rank=%d, received data (%d) mismatches expected data (%d)", rank, data, rank);
    }

    MPI_Finalize();
    return 0;
}
