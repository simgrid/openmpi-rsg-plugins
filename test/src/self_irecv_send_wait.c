#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    printf("Rank=%d. ENV(RsgRpcNetworkName) = %s\n",
        rank, getenv("RsgRpcNetworkName"));

    // Data: an array of 1000 integers. All values should equal rank.
    const int array_size = 1000;
    int * send_array = calloc(array_size, sizeof(int));
    int * recv_array = calloc(array_size, sizeof(int));

    // Initialize send_array and recv_array
    for (int i = 0; i < array_size; i++)
    {
        send_array[i] = rank;
        recv_array[i] = -1;
    }

    // Precheck
    for (int i = 0; i < array_size; i++)
    {
        assertf(send_array[i] == rank, "rank=%d. cell %d of send_array is %d (expected %d)", rank, i, send_array[i], rank);
        assertf(recv_array[i] == -1, "rank=%d. cell %d of recv_array is %d (expected %d)", rank, i, recv_array[i], -1);
    }

    // Send data to ourself
    MPI_Request request;
    MPI_Irecv(recv_array, array_size, MPI_INT, rank, 0, MPI_COMM_WORLD, &request);
    MPI_Send(send_array, array_size, MPI_INT, rank, 0, MPI_COMM_WORLD);
    MPI_Wait(&request, MPI_STATUS_IGNORE);

    // Postcheck
    for (int i = 0; i < array_size; i++)
    {
        assertf(send_array[i] == rank, "rank=%d. cell %d of send_array is %d (expected %d)", rank, i, send_array[i], rank);
        assertf(recv_array[i] == rank, "rank=%d. cell %d of recv_array is %d (expected %d)", rank, i, recv_array[i], rank);
    }

    free(send_array);
    free(recv_array);

    MPI_Finalize();
    return 0;
}
