#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Data: an array of 1000 integers. All values should equal rank.
    const int array_size = 1000;
    int * send_array1 = calloc(array_size, sizeof(int));
    int * send_array2 = calloc(array_size, sizeof(int));
    int * recv_array1 = calloc(array_size, sizeof(int));
    int * recv_array2 = calloc(array_size, sizeof(int));

    // Initialize send_array and recv_array
    for (int i = 0; i < array_size; i++)
    {
        send_array1[i] = rank;
        send_array2[i] = rank + 1;
        recv_array1[i] = -1;
        recv_array2[i] = -1;
    }

    // Precheck
    for (int i = 0; i < array_size; i++)
    {
        assertf(send_array1[i] == rank, "rank=%d. cell %d of send_array1 is %d (expected %d)", rank, i, send_array1[i], rank);
        assertf(send_array2[i] == rank+1, "rank=%d. cell %d of send_array2 is %d (expected %d)", rank, i, send_array2[i], rank+1);
        assertf(recv_array1[i] == -1, "rank=%d. cell %d of recv_array1 is %d (expected %d)", rank, i, recv_array1[i], -1);
        assertf(recv_array2[i] == -1, "rank=%d. cell %d of recv_array1 is %d (expected %d)", rank, i, recv_array2[i], -1);
    }

    // Send data to ourself
    MPI_Request requests[2];
    MPI_Status statuses[2];
    MPI_Isend(send_array1, array_size, MPI_INT, rank, 1, MPI_COMM_WORLD, &requests[0]);
    MPI_Isend(send_array2, array_size, MPI_INT, rank, 2, MPI_COMM_WORLD, &requests[1]);
    MPI_Recv(recv_array1, array_size, MPI_INT, rank, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(recv_array2, array_size, MPI_INT, rank, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Waitall(2, requests, statuses);

    // Postcheck
    int first1 = send_array1[0];
    assertf((first1 == rank) || (first1 == rank + 1), "rank=%d. cell 0 of send_array1 is %d (expected %d or %d)", rank, first1, rank, rank+1);
    int first2 = send_array2[0];
    assertf((first2 == rank) || (first2 == rank + 1), "rank=%d. cell 0 of send_array2 is %d (expected %d or %d)", rank, first2, rank, rank+1);
    assertf(first1 != first2, "rank=%d. inconsistency in first value of received arrays (got %d and %d but they should be in {%d, %d}", rank, first1, first2, rank, rank+1);

    for (int i = 0; i < array_size; i++)
    {
        assertf(send_array1[i] == rank, "rank=%d. cell %d of send_array1 is %d (expected %d)", rank, i, send_array1[i], rank);
        assertf(send_array2[i] == rank+1, "rank=%d. cell %d of send_array2 is %d (expected %d)", rank, i, send_array2[i], rank+1);
        assertf(recv_array1[i] == first1, "rank=%d. cell %d of recv_array1 is %d (expected %d)", rank, i, recv_array1[i], first1);
        assertf(recv_array2[i] == first2, "rank=%d. cell %d of recv_array2 is %d (expected %d)", rank, i, recv_array2[i], first2);
    }

    free(send_array1);
    free(send_array2);
    free(recv_array1);
    free(recv_array2);

    MPI_Finalize();
    return 0;
}
