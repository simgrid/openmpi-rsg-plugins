image: simgrid/openmpi-rsg-plugins_ci

variables:
  GIT_SUBMODULE_STRATEGY: none
  OMPI_SRC_PATH: /openmpi
  OMPI_INSTALL_PATH: /openmpi/install
  RSG_INSTALL_PATH: /usr
  SG_INSTALL_PATH: /usr
  MPIRUN_BASE_CMD: /openmpi/install/bin/mpirun --allow-run-as-root --mca btl ^vader

stages:
  - build
  - test

###############################################################################
# Build stage
###############################################################################
build_test_apps:
  stage: build
  script:
    - cd test
    - CC=/openmpi/install/bin/mpicc meson build
    - ninja -C build
  artifacts:
    paths:
      - /builds/simgrid/openmpi-rsg-plugins/test/build

build_mpich3_test_apps:
  stage: build
  script:
    - cd test/mpich3
    - CC=/openmpi/install/bin/mpicc meson build
    - ninja -C build
  artifacts:
    paths:
      - /builds/simgrid/openmpi-rsg-plugins/test/mpich3/build

build_plugins:
  stage: build
  script:
    - mkdir -p build
    - cd build
    - cmake ..
            -DOMPI_SRC_PATH=${OMPI_SRC_PATH}
            -DOMPI_INSTALL_PATH=${OMPI_INSTALL_PATH}
            -DRSG_INSTALL_PATH=${RSG_INSTALL_PATH}
            -DSimGrid_PATH=${SG_INSTALL_PATH}
            -Denable_debug_odls_rsg=ON
            -Denable_debug_pml_rsg=ON
            -DCMAKE_INSTALL_PREFIX=${OMPI_INSTALL_PATH}
    - make -j 4
  artifacts:
    paths:
      - /builds/simgrid/openmpi-rsg-plugins/build

###############################################################################
# Test stage
###############################################################################
test_no_plugin_np2:
  stage: test
  script:
    - cd /builds/simgrid/openmpi-rsg-plugins/test
    - meson test --list -C build
    - ./run_tests.bash -w "${MPIRUN_BASE_CMD} -np 2 --mca odls default --mca pml ob1" hello barrier self_irecv_send_wait self_irecv_isend_waitall self_irecv_isend_waitall_barrier self_isend_recv_wait self_isend_irecv_waitall self_isend_irecv_waitall_barrier self_isend_recv_wait_loop self_isend2_recvany self2_isend_irecv_waitall_barrier oth_send_recv oth_send_recv_loop oth_isend_irecv_waitall oth2_isend_recv_wait gather gather_type_stride scatter
  dependencies:
    - build_test_apps

test_experimental_mpich3_no_plugin_np2:
  allow_failure: true
  stage: test
  script:
    - cd /builds/simgrid/openmpi-rsg-plugins/test
    - TESTS=$(find mpich3/build -executable -type f | sort -u | grep -v meson | sed -E 'sWmpich3/build/WW' | tr '\n' ' ')
    - ./run_tests.bash -b './mpich3/build' -w "${MPIRUN_BASE_CMD} -np 2 --mca odls default --mca pml ob1" ${TESTS}
  dependencies:
    - build_mpich3_test_apps

test_np2:
  stage: test
  script:
    # install the plugins
    - cd /builds/simgrid/openmpi-rsg-plugins/build
    - make install
    # run the tests
    - cd /builds/simgrid/openmpi-rsg-plugins/test/
    - meson test --list -C build
    - export RSG_SERVER_DEPLOYMENT_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/deploy/deploy2.xml
    - export RSG_SERVER_PLATFORM_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/cluster512.xml
    - ./run_tests.bash -w "${MPIRUN_BASE_CMD} --mca async_mpi_finalize 1 -np 2 --mca odls rsg --mca pml rsg" hello
  dependencies:
    - build_test_apps
    - build_plugins

test_experimental_np2:
  allow_failure: true
  stage: test
  script:
    # install the plugins
    - cd /builds/simgrid/openmpi-rsg-plugins/build
    - make install
    # run the tests
    - cd /builds/simgrid/openmpi-rsg-plugins/test/
    - meson test --list -C build
    - export RSG_SERVER_DEPLOYMENT_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/deploy/deploy2.xml
    - export RSG_SERVER_PLATFORM_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/cluster512.xml
    - ./run_tests.bash -w "${MPIRUN_BASE_CMD} --mca async_mpi_finalize 1 -np 2 --mca odls rsg --mca pml rsg" hello barrier self_irecv_send_wait self_irecv_isend_waitall self_irecv_isend_waitall_barrier self_isend_recv_wait self_isend_irecv_waitall self_isend_irecv_waitall_barrier self_isend_recv_wait_loop self_isend2_recvany self2_isend_irecv_waitall_barrier oth_send_recv oth_send_recv_loop oth_isend_irecv_waitall oth2_isend_recv_wait gather gather_type_stride scatter
  dependencies:
    - build_test_apps
    - build_plugins

test_experimental_np1:
  allow_failure: true
  stage: test
  script:
    # install the plugins
    - cd /builds/simgrid/openmpi-rsg-plugins/build
    - make install
    # run the tests
    - cd /builds/simgrid/openmpi-rsg-plugins/test/
    - meson test --list -C build
    - export RSG_SERVER_DEPLOYMENT_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/deploy/deploy1.xml
    - export RSG_SERVER_PLATFORM_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/cluster512.xml
    - ./run_tests.bash -w "${MPIRUN_BASE_CMD} --mca async_mpi_finalize 1 -np 1 --mca odls rsg --mca pml rsg" hello barrier self_irecv_send_wait self_irecv_isend_waitall self_irecv_isend_waitall_barrier self_isend_recv_wait self_isend_recv_wait_loop self_isend_irecv_waitall self_isend_irecv_waitall_barrier self_isend2_recvany gather gather_type_stride scatter
  dependencies:
    - build_test_apps
    - build_plugins

test_experimental_mpich3_np2:
  allow_failure: true
  stage: test
  script:
    # install the plugins
    - cd /builds/simgrid/openmpi-rsg-plugins/build
    - make install
    # run the tests
    - cd /builds/simgrid/openmpi-rsg-plugins/test
    - TESTS=$(find mpich3/build -executable -type f | sort -u | grep -v meson | sed -E 'sWmpich3/build/WW' | tr '\n' ' ')
    - export RSG_SERVER_DEPLOYMENT_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/deploy/deploy2.xml
    - export RSG_SERVER_PLATFORM_FILENAME=/builds/simgrid/openmpi-rsg-plugins/test/resources/cluster512.xml
    - ./run_tests.bash -b './mpich3/build' -w "${MPIRUN_BASE_CMD} --mca async_mpi_finalize 1 -np 2 --mca odls rsg --mca pml rsg" ${TESTS}
  dependencies:
    - build_mpich3_test_apps
    - build_plugins
