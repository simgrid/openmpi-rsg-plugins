How to get an OpenMPI installation with RSG plugins?
====================================================
In a nutshell:
1. Build OpenMPI. Install it. **Keep** build directory.
2. Build OpenMPI-RSG plugins, using some of OpenMPI's headers.
3. Put OpenMPI-RSG plugins into OpenMPI's install directory.

This document proposes two guides to install OpenMPI + our plugins.
- [Manually](#manually) (recommended but error-prone)
- [Using Nix](#using-nix) (may not work depending on kernel)

Manually
--------

### Dependencies
First, install all needed dependencies. Quick non-exhaustive list:
- For OpenMPI's build system: autoconf, libtool, automake, flex
- To build OpenMPI: C/FORTRAN compiler, perl, (rdma-core, libnl, zlib)
- For SimGrid build system: cmake
- For SimGrid: boost, perl, python, (elfutils)
- For Remote SimGrid: ZeroMQ (C and C++), Apache Thrift 0.11.0
- For Openmpi-RSG plugins: jansson

**As I write these lines, the recommended SimGrid commit is b882330, and the recommended RSG commit is 8087625.**

### Build OpenMPI
``` bash
# Get the source
wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.1.tar.bz2
tar -xf openmpi-3.1.1.tar.bz2
# git clone https://github.com/open-mpi/ompi.git

# Get into OpenMPI's directory
cd openmpi-3.1.1

# IMPORTANT: ALWAYS build from a clean repository.
#            If you changed any system dependency, restart the whole process
#            (or prepare yourself, intricate bugs are coming)

# (re)generate the configure script.
# DO NOT USE the ./configure bundled in OpenMPI releases!
./autogen.pl --force

# Configure. Specify where you want to install OpenMPI with --prefix
# NOTE: OpenMPI must be built with dynamic plugins. Therefore, DO NOT use
#       https://www.open-mpi.org/faq/?category=building#avoid-dso
# NOTE: Set the final destination with --prefix.
#       Otherwise, OpenMPI install path must be hacked in OpenMPI binaries.
#       Please refer to the Nix recipes if you need to do such hacks.
./configure --prefix=$(realpath ./install) # (may take a long time)

# Build OpenMPI
make # (may take a long time)

# Install OpenMPI
make install
```

### Build SimGrid
``` bash
# Get the source
git clone https://github.com/simgrid/simgrid.git
# (select the right commit)

# Get into SimGrid's directory
cd simgrid

# "Configure"
mkdir -p build && cd build
cmake ..

# Build
make

# Install
make install
```

### Build Remote SimGrid
``` bash
# Get the source
git clone https://github.com/simgrid/remote-simgrid.git
# (select the right commit)

# Get into Remote SimGrid's directory
cd remote-simgrid

# "Configure"
mkdir -p build && cd build
cmake ..

# Build
make

# Install
make install
```

### Build OpenMPI-RSG plugins
``` bash
# Get the source
git clone https://framagit.org/simgrid/openmpi-rsg-plugins.git

# Get into OpenMPI-RSG plugins's directory
cd openmpi-rsg-plugins

# Set some environment variables
export SimGrid_PATH=/where/simgrid/is/installed
export RSG_INSTALL_PATH=/where/remote-simgrid/is/installed
export OMPI_INSTALL_PATH=/where/openmpi/is/installed
export OMPI_SRC_PATH=/openmpi/sources/root/directory

# "Configure". Set OpenMPI's install path as CMAKE_INSTALL_PREFIX
mkdir -p build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=${OMPI_INSTALL_PATH}

# Build the plugins
make

# Put the plugins into OpenMPI's install directory (so it can find them)
make install
```

### Runtime setup
In a manual installation, more setup may be required to execute the plugins
depending on where they have been installed.
- Make sure Remote SimGrid programs are in your `PATH`
- Make sure all the libraries required by the plugins are in your `LD_LIBRARY_PATH`

Using Nix
---------
First, install Nix if needed.
Up-to-date information on installing Nix can be found
[there](https://nixos.org/nix/). Usually, this is as simple as:
``` bash
curl https://nixos.org/nix/install | sh
# Then follow instructions at the end of the script.
# Usually: source ~/nix-profiles/etc/profile.d/nix.sh
```

You can then easily get into a shell in which OpenMPI is installed with
our plugins:
``` bash
# Retrieve OpenMPI-RSG plugins repo if needed
git clone https://framagit.org/simgrid/openmpi-rsg-plugins.git

# Go at the clone's root directory
cd openmpi-rsg-plugins

# Get into a shell with OpenMPI+plugins.
# Note: Nix will build ALL needed dependencies on first run
#       - OpenMPI and its dependencies
#       - Our plugins' dependencies (SimGrid, Remote SimGrid, Thrift...)
# If you prefer to use a binary cache, read cachix section below
nix-shell shell.nix # coffee break!
```

If you prefer to install our OpenMPI+plugins bundle in your system:
``` bash
nix-env -f default.nix -iA openmpi_rsg
```

### How to use SimGrid's cachix binary cache?
Nix proposes recipes that can fully rebuild the whole software stack.
This does not mean everything must be built on the end-user's machine.
By default Nix will look for packages in the standard binary cache, but users
can add SimGrid's binary cache to avoid costly builds:

``` bash
# Install cachix
nix-env -iA cachix -f https://github.com/NixOS/nixpkgs/tarball/1d4de0d552ae9aa66a5b8dee5fb0650a4372d148
# Tell cachix to use simgrid.cachix.org
cachix use simgrid
```
