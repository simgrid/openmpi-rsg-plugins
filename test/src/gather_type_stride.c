#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int errs = 0;
    int rank = -1, size = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    for (int count = 1; count < 5; count = count * 2)
    {
        int root = 0;
        int n = 12;
        int stride = 10;
        double * vecin = (double *) malloc(n * stride * size * sizeof(double));
        double * vecout = (double *) malloc(size * n * sizeof(double));

        MPI_Datatype vec;
        MPI_Type_vector(n, 1, stride, MPI_DOUBLE, &vec);
        MPI_Type_commit(&vec);

        for (int i = 0; i < n * stride; i++)
            vecin[i] = -2;
        for (int i = 0; i < n; i++)
            vecin[i * stride] = rank * n + i;

        MPI_Gather(vecin, 1, vec, vecout, n, MPI_DOUBLE, root, MPI_COMM_WORLD);

        if (rank == root)
        {
            for (int i = 0; i < n * size; i++)
            {
                if (vecout[i] != i)
                {
                    errs++;
                    printf("count=%d, vecout[%d]=%d\n", count, i, (int) vecout[i]);
                }
            }
        }
        MPI_Type_free(&vec);
        free(vecin);
        free(vecout);
    }

    MPI_Finalize();

    return errs > 0;
}
