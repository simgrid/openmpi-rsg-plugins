{ stdenv, fetchgit,
  openmpi_rsg_plugins, simgrid, remote_simgrid, openmpi }:

(openmpi_rsg_plugins.override {inherit simgrid; inherit remote_simgrid;
  inherit openmpi; }).overrideAttrs (oldAttrs: rec {
  name = "openmpi-rsg-plugins-${version}";
  version = "local";

  patches = [];

  src = stdenv.lib.sourceByRegex ./. [
      "^src$"
      "^src/btl$"
      "^src/btl/self_rsg$"
      "^src/btl/tcp_rsg$"
      "^src/odls$"
      "^src/odls/rsg$"
      "^src/pml$"
      "^src/pml/rsg$"
      "^test$"
      "^test/.*$"
      "^test/.*/.*$"
        ".*\.cpp$" ".*\.hpp$"
        ".*\.c$" ".*\.h$"
      "^cmake$"
        ".*\.cmake"
        ".*\.cmake.in"
      ".*CMakeLists\.txt$"
    ];
  }
)
