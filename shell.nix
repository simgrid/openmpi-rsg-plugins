{
  pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {},
  localpkgs ? import ./default.nix {},
}:

pkgs.stdenv.mkDerivation rec {
  name = "testEnv";
  propagatedBuildInputs = [
    localpkgs.openmpi_rsg
  ];
  # Define runtime environment variables for convenience
  RSG_SERVER_PLATFORM_FILENAME=./test/resources/cluster512.xml;
  RSG_SERVER_DEPLOYMENT_FILENAME=./test/resources/deploy/deploy2.xml;
}
